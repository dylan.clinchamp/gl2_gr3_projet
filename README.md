# Projet "Hashiwokakero" consistant à réaliser un jeu de Hashi en Ruby avec gtk3
### Projet de troisième année de Licence Informatique

### Pré-requis :
    - Ruby 2.7 ou Ruby 3.x
    - Avoir installé les Ruby Gems suivantes : gtk3, activerecord, sqlite3

### Lancement :    
    ==> Exécuter le fichier Alias_Lancement.rb situé dans src


#### Auteurs : 
#### CLINCHAMP DYLAN 
#### DANJOU ALEXANDRE 
#### FATNASSI MENDY 
#### MOTTAY MATHILDE 
#### MUNAR MARIE-NINA
#### PROUDY JULIEN
#### UFACIK FATIH

